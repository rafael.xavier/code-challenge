<?php

if (!function_exists('area')) {
    /**
     * @param float $float
     * @return float
     */
    function area($largura, $altura): float
    {
        return $largura * $altura;
    }
}

if (!function_exists('calculaLatas')) {

    function calculaLatas(float $litrosOriginal)
    {
        $litros = $litrosOriginal;
        $tamanhoLatas = getTamanhoLatas();
        $result = [];

        foreach ($tamanhoLatas as $latas) {
            while ($litros >= $latas) {
                if (!array_key_exists($latas, $result)) {
                    $result[$latas] = 0;
                }
                $result[$latas]++;
                $litros = (float) $litros - (float) $latas;
            }
        }

        if ($litros > 0) {
            if (!array_key_exists("0.5", $result)) {
                $result[$latas] = 0;
            }
            $result[$latas]++;
        }

        return imprimeResultado($litrosOriginal, $result);
    }

    function getTamanhoLatas()
    {
        return array(
            "18.0",
            "3.6",
            "2.5",
            "0.5",
        );
    }

    function imprimeResultado($litrosOriginal, $result)
    {
        $mensagem = 'Para ' . round($litrosOriginal, 1) . ' Litros você precisará de:';
        $tamanhoArray = count($result);
        $contArray = 1;
        foreach ($result as $tamanhoLatas => $quantidade) {
            if ($contArray == 1) {
                $mensagem .= " $quantidade lata(s) de $tamanhoLatas L";
            } else {
                if ($contArray == $tamanhoArray) {
                    $mensagem .= " e $quantidade lata(s) de $tamanhoLatas L";
                } else {
                    $mensagem .= ", $quantidade lata(s) de $tamanhoLatas L";
                }
            }
            $contArray++;
        }

        $return['message'] = $mensagem;

        return json_encode($return);
    }

}
