<?php

namespace App\Services;

class PintarSalaService
{
    public function __construct()
    {
        $this->larguraJanela = 2;
        $this->alturaJanela = 1.2;
        $this->larguraPorta = 0.8;
        $this->alturaPorta = 1.9;
        $this->metrosPorLitro = 5;
    }
    public function pintarSala($request)
    {
        $areaTotalLivre = 0;
        foreach ($request->sala as $paredes) {
            $areaParede = area($paredes['largura'], $paredes['altura']);
            $areaJanelas = area($this->larguraJanela, $this->alturaJanela) * $paredes['qtdJanelas'];
            $areaPortas = area($this->larguraPorta, $this->alturaPorta) * $paredes['qtdPortas'];
            $areaOcupadaPerc = (($areaJanelas + $areaPortas) / $areaParede) * 100;
            $areaTotalLivre += $areaParede - ($areaJanelas + $areaPortas);
            $return = false;

            $return = $this->validaTamanho($areaParede, $paredes['nome']);
            if ($return) {
                return $return;
            }

            $return = $this->validaPercentualOcupado($areaOcupadaPerc, $paredes['nome']);
            if ($return) {
                return $return;
            }

            $return = $this->validaAlturaParedePorta($paredes['qtdPortas'], $paredes['altura'], $paredes['nome']);
            if ($return) {
                return $return;
            }

        }

        return $this->calculaQuantidadeLatas($areaTotalLivre);
    }

    public function validaTamanho($areaParede, $nome)
    {
        if ($areaParede < 1 || $areaParede > 15) {
            return response()->json([
                'parede' => $nome,
                'message' => 'Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados. A área da parede está em ' . number_format($areaParede, 2, ',', '.') . 'M²',
            ], 400);
        }
    }

    public function validaPercentualOcupado($areaOcupadaPerc, $nome)
    {
        if ($areaOcupadaPerc > 50) {
            return response()->json([
                'parede' => $nome,
                'message' => 'O total de área das portas e janelas devem ser no máximo 50% da área da parede. O percentual está em ' . number_format($areaOcupadaPerc, 2, ',', '.') . '%',
            ], 400);
        }
    }

    public function validaAlturaParedePorta($qtdPortas, $altura, $nome)
    {
        $alturaParedePorta = $qtdPortas ? ($altura - $this->alturaPorta) * 100 : 0;
        if ($qtdPortas > 0 && $alturaParedePorta < 30) {
            return response()->json([
                'parede' => $nome,
                'message' => 'A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta. A altura entre a parede e a porta está em ' . number_format($alturaParedePorta, 2, ',', '.') . '(cm)',
            ], 400);
        }
    }

    public function calcularLitrosNecessarios($areaDisponivel)
    {
        return ($areaDisponivel / $this->metrosPorLitro);
    }

    public function calculaQuantidadeLatas($areaDisponivel)
    {
        $litros = $this->calcularLitrosNecessarios($areaDisponivel);

        return calculaLatas($litros);
    }
}
