<?php
namespace App\Swagger\ParedesRequest;

/**
 * @OA\Schema(schema="ParedesRequest")
 */

class ParedesRequest
{

    /**
     * @OA\Property(type="string")
     *
     * @var string
     */
    public $nome;
    
    /**
     * @OA\Property(type="number")
     *
     * @var float
     */
    public $altura;
    
    /**
     * @OA\Property(type="number")
     *
     * @var float
     */
    public $largura;
    
    /**
     * @OA\Property(type="number")
     *
     * @var int
     */
    public $qtdPortas;
    
    /**
     * @OA\Property(type="number")
     *
     * @var int
     */
    public $qtdJanelas;
}