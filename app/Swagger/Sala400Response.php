<?php
namespace App\Swagger\Sala400Response;

/**
 * @OA\Schema(schema="Sala400Response")
 */

class Sala400Response
{

    /**
     * @OA\Property(type="string")
     *
     * @var string
     */
    public $parede;
    
    /**
     * @OA\Property(type="string")
     *
     * @var string
     */
    public $message;
    
}
