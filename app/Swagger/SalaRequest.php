<?php
namespace App\Swagger\SalaRequest;
/**
 * @OA\Schema(schema="SalaRequest")
 */

class SalaRequest
{
    
    /**
     * @OA\Property(type="array", example={{
*			"nome": "parede_1",
*			"altura": 2.3,
*			"largura": 4,
*			"qtdPortas": 1,
*			"qtdJanelas": 1
*		},
*		{
*			"nome": "parede_2",
*			"altura": 3,
*			"largura": 5,
*			"qtdPortas": 1,
*			"qtdJanelas": 2
*		},
*		{
*			"nome": "parede_3",
*			"altura": 2.5,
*			"largura": 2.8,
*			"qtdPortas": 1,
*			"qtdJanelas": 0
*		},
*		{
*			"nome": "parede_4",
*			"altura": 3,
*			"largura": 3,
*			"qtdPortas": 1,
*			"qtdJanelas": 1
*		}},@OA\Items(ref="#/components/schemas/ParedesRequest"))
     *
     * @var array
     */
    public $sala;
}
