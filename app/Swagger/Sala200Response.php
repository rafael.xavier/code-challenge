<?php
namespace App\Swagger\Sala200Response;

/**
 * @OA\Schema(schema="Sala200Response")
 */

class Sala200Response
{

    /**
     * @OA\Property(type="string")
     *
     * @var string
     */
    public $message;
    
}
