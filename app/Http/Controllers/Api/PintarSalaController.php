<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\PintarSalaService;
use Illuminate\Http\Request;

class PintarSalaController extends Controller
{
    public function __construct(PintarSalaService $pintarSalaService)
    {
        $this->PintarSalaService = $pintarSalaService;
    }

    /**
     * @OA\Post(
     *      path="/api/pintar-sala",
     *      tags={"Pintar Sala"},
     *      summary="Calcula a quantidade de ................",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/SalaRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Sala200Response")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(ref="#/components/schemas/Sala400Response")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    public function pintarSala(Request $request)
    {
        return $this->PintarSalaService->pintarSala($request);
    }
}
