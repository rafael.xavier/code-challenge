<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Code Challenge",
 *      description="O Objetivo dessa aplicação web é ajudar usuário a calcular a quantidade de tinta necessária para pintar uma sala. Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede. Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L.<br><br><strong>Regras de negócio</strong>
 * Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes
 * O total de área das portas e janelas deve ser no máximo 50% da área de parede
 * A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
 * Cada janela possui as medidas: 2,00 x 1,20 mtos
 * Cada porta possui as medidas: 0,80 x 1,90
 * Cada litro de tinta é capaz de pintar 5 metros quadrados
 * Não considerar teto nem piso.<br> <br> As variações de tamanho das latas de tinta são: <br><br>0,5 L<br>2,5 L<br>3,6 L<br>18 L",
 *      @OA\Contact(
 *          email="rafaellessacastro@hotmail.com"
 *      )
 * )
 *
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
