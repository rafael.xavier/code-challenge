<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace ('App\Http\Controllers\Api')
    ->group(function () {
        Route::post('pintar-sala', 'PintarSalaController@pintarSala');
    });
