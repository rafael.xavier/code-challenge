FROM php:7-apache

# Instalação inicial de dependencias do Container
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libjpeg-dev \
    libfreetype6-dev \
    zip \
    unzip \
    zlibc
# Instalando extensões do php
RUN docker-php-ext-configure gd --with-freetype --with-jpeg\
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install pdo_mysql mbstring

# Instalando Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Habilitando o módulo de reescrita do Apache
RUN a2enmod rewrite
RUN sed -i 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/000-default.conf

# Copiando back-end da aplicação para o apache
COPY ./ /var/www/html

# Instalando Composer
RUN composer install

# Muda o usuario dono da pasta e arquivos recursivamente
RUN chown -R www-data storage/

# Permissão para evitar erro de escrita de LOG(Permissão de acesso)
RUN chmod -R 777 /var/www/html/storage/logs/

# Expondo porta interna do servidor
EXPOSE 80

